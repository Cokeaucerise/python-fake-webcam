#!/usr/bin/env python3

import sys
import subprocess
import re
from io import StringIO
from Xlib import X, display, Xutil
from subprocess import Popen

if __name__ == '__main__':

    if len(sys.argv) == 2 and sys.argv[1] == '-S':
        print(Popen("kill \"$(ps -ea | grep -i 'streamlink' | awk '{print $1}')\"", shell=True, stdout=subprocess.PIPE).stdout.readline().decode())
    else:
        DEVICES = Popen("ls -1 /dev/video*", shell=True, stdout=subprocess.PIPE).stdout.readlines()
        DEVICES = list(map(lambda s: s.decode().strip(), DEVICES))
        ROFI_IN = '\n'.join(DEVICES)

        SEL = Popen("echo '{}' | rofi -dmenu -p 'Select Dummy webcam' -columns 2 -a 0 -no-custom -font 'icomoon\-feather 10'".format(ROFI_IN),
            shell=True, stdout=subprocess.PIPE
        ).stdout.readlines()
        SEL_DEVICE = SEL[0].decode().strip()

        URL = Popen("echo '' | rofi -dmenu -p 'Enter stream url' -lines 0 -columns 1 -a 0 -font 'icomoon\-feather 10'",
            shell=True, stdout=subprocess.PIPE
        ).stdout.readlines()
        STREAM_URL = URL[0].decode().strip()

        Popen('python3 %s -S' % sys.argv[0], shell=True, stdout=subprocess.PIPE).stdout.readline().decode().strip('\n')

        streamlink_command = 'streamlink -O {} best'.format(STREAM_URL)
        streamlink_command += '| ffmpeg -y -re -i pipe:0 -map 0:v -codec rawvideo -r 25 -an -pix_fmt yuv420p -f v4l2 {}'.format(SEL_DEVICE)

        print(streamlink_command)
        Popen(streamlink_command, shell=True)
